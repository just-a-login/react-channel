import React from "react";
import ReactDOM from "react-dom";
import {Router, Route, hashHistory} from 'react-router'
import {Provider} from 'react-redux';
import store from "./store";

import styles from "./reset.css";
import ThreadScreen from "./source/components/ThreadScreen";


class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Router history={hashHistory}>
                    <Route path="/" component={ThreadScreen}/>
                </Router>
            </Provider>
        );
    }
}

ReactDOM.render(<App/>, document.getElementById('app-container'));
