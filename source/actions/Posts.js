/**
 * Created by JustLogin on 13.12.2016.
 */

import axios from 'axios';
import {ALL_POSTS, ONE_POST} from './TempConst';


export const REQUEST_PROGRESS = "requestProgress";
export const ADD_POSTS = "addPosts";


function requestProgress(flag) {
    return {
        type: REQUEST_PROGRESS,
        data: flag
    }
}

function returnPosts(resp, actionType) {
    let posts = {};
    for (let i = 0; i < resp.length; i++) {
        posts[resp[i].num] = {
            text: resp[i].comment,
            answers: []
        };
    }

    return {
        type: actionType,
        data: posts
    };
}


export function requestPosts() {
    return dispatch => {
        dispatch(requestProgress(true));
        dispatch(returnPosts(ALL_POSTS, ADD_POSTS));
        dispatch(requestProgress(false));
    };
}

export function addPosts() {
    return dispatch => {
        dispatch(requestProgress(true));
        dispatch(returnPosts(ONE_POST, ADD_POSTS));
        dispatch(requestProgress(false));
    };
}