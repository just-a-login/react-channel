/**
 * Created by JustLogin on 11.12.2016.
 */

import {REQUEST_PROGRESS, ADD_POSTS} from "../actions/Posts";
import {assign as _assign} from 'lodash-es';


let initialState = {
    posts: {},
    inProgress: false
};

export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_POSTS:
            let posts = _assign({}, action.data, state.posts);
            for (let key in action.data) {
                let parents = action.data[key].text.match(/>>\d+/);
                if (parents) {
                    for (let j = 0; j < parents.length; j++) {
                        parents[j] = parents[j].substring(2);
                        if (posts[parents[j]]) posts[parents[j]].answers.push(key);
                    }
                }
            }
            return _assign({}, state, {posts: posts});
        case REQUEST_PROGRESS:
            return _assign({}, state, {inProgress: action.data});
        default:
            return state;
    }
};