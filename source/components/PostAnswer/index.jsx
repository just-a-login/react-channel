import React from "react";
import styles from './index.css';


export default class PostAnswer extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <a className={styles.mainA} href={'#' + this.props.id}>{'>>' + this.props.id}</a>
        );
    }
}