import React from "react";
import {connect} from "react-redux";
import Post from '../Post';
import {requestPosts, addPosts} from '../../actions/Posts';


class ThreadScreen extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <div>
                {!this.props.inProgress && <button onClick={this.getPosts.bind(this)}>GET POSTS</button>}
                <button onClick={() => this.props.dispatch(addPosts())}>ADD POSTS</button>
                {this.renderPosts()}
            </div>
        );
    }

    renderPosts() {
        let posts = [];
        for (let key in this.props.posts) {
            posts.push(<Post key={key} id={key} text={this.props.posts[key].text} answers={this.props.posts[key].answers}/>);
        }

        return posts;
    }

    getPosts() {
        this.props.dispatch(requestPosts());
    }
}


export default connect((store) => {
    return {
        posts: store.CurrentThread.posts,
        inProgress: store.CurrentThread.inProgress
    };
})(ThreadScreen);
