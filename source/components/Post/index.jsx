import React from "react";
import styles from "./index.css";
import PostAnswer from '../PostAnswer'


export default class Post extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <div className={styles.mainDiv}>
                <div>{this.props.id}</div>
                <div dangerouslySetInnerHTML={{__html: this.props.text}}/>
                {this.props.answers.length != 0 && <div>
                    <span>Ссылки: </span>
                    <span>{this.props.answers.map(item => {return <PostAnswer key={item} id={item}/>})}</span>
                </div>}
            </div>
        );
    }
}