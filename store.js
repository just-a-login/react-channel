import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from './source/reducers';

export default createStore(reducers, applyMiddleware(thunk));
